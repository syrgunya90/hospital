import psycopg2
from contextlib import closing


def read_hospital_details(ids):
    with closing(psycopg2.connect("dbname=test2 user=postgres password=postgres123 host=127.0.0.1 port=5432")) as conn:
        with conn.cursor() as cursor:
            cursor.execute("SELECT hospital_id, hospital_name, bed_count FROM hospitals WHERE hospital_id = %s", (ids,))
            print("Hospital Params:")
            i = 0;
            for row in cursor:
                i = i+1
                print("Hospital ID:", row[0])
                print("Hospital Name:", row[1])
                print("Bed Count:", row[2])
                print("")
            if i == 0:
                print("По введенной айди нет госпеталей")


def read_doctor_details(ids):
    with closing(psycopg2.connect("dbname=test2 user=postgres password=postgres123 host=127.0.0.1 port=5432")) as conn:
        with conn.cursor() as cursor:
            cursor.execute("SELECT doctor_id, doctor_name, hospital_id, joining_date, speciality, salary, experience "
                           "FROM doctors WHERE doctor_id = %s", (ids,))
            print("Doctor Params:")
            i = 0;
            for row in cursor:
                i = i + 1
                print("Doctor ID:", row[0])
                print("Doctor Name:", row[1])
                print("Hospital Id:", row[2])
                print("Joining Date:", row[3])
                print("Speciality:", row[4])
                print("Salary:", row[5])
                print("Experience:", row[6])
                print("")
            if i == 0:
                print("По введенной айди докторов нет")


def get_doct_ord_within_hospital(ids):
    with closing(psycopg2.connect("dbname=test2 user=postgres password=postgres123 host=127.0.0.1 port=5432")) as conn:
        with conn.cursor() as cursor:
            cursor.execute("SELECT d.doctor_id, d.doctor_name, d.hospital_id, h.hospital_name, d.joining_date, "
                           "d.speciality, d.salary, d.experience FROM doctors d, hospitals h WHERE  d.hospital_id = %s "
                           "and h.hospital_id = d.hospital_id", (ids,))
            print("Hospital ID: ", ids)
            i = 0;
            for row in cursor:
                i = i + 1
                print("Doctor ID:", row[0])
                print("Doctor Name:", row[1])
                print("Hospital ID:", row[2])
                print("Hospital Name:", row[3])
                print("Joining Date:", row[4])
                print("Speciality:", row[5])
                print("Salary:", row[6])
                print("Experience:", row[7])
                print("")
            if i == 0:
                print("По введенной айди нет госпиталей")


def update_doctors_experience(ids):
    with closing(psycopg2.connect("dbname=test2 user=postgres password=postgres123 host=127.0.0.1 port=5432")) as conn:
        with conn.cursor() as cursor:
            cursor.execute("update doctors set experience = date_part from "
                           "(select doctor_id, date_part('year', age(joining_date)) "
                           "from doctors  where doctor_id = %s) "
                           "AS years where doctors.doctor_id = years.doctor_id", (ids,))
            print("Experience Updated")


act = int(input("Что отобразить? 1: Детали госпиталя. 2: Детали доктора. 3: Список врачей которые работают в "
                "госпитале. 4: Высчитать и/или обновить стаж доктора "))
if act == 1:
    ids = int(input("Вы выбрали детали госпиталя: Введите айди госпиталя "))
    read_hospital_details(ids)
elif act == 2:
    ids = int(input("Вы выбрали детали доктора: Введите айди доктора "))
    read_doctor_details(ids)
elif act == 3:
    ids = int(input("Вы выбрали список врачей которые работают в госпитале: Введите айди госпиталя "))
    get_doct_ord_within_hospital(ids)
elif act == 4:
    ids = int(input("Вы выбрали высчитать и/или обновить стаж доктора: Введите айди доктора "))
    update_doctors_experience(ids)
else:
    print("Введенные Вами данные не соответсвуют требуемым ")




